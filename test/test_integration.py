import unittest

import yaml

from official_build import build


MANIFEST_FIXTURE = """
repositoryPath: 'atlassian/ssh-run'
version: '0.2.6'
"""

MANIFEST_CUSTOM_PIPE_OLD_METADATA_FIXTURE = """name: WhiteSource scan
description: Scan and report open sources vulnerabilities and security issues
category: Security
logo: 'https://bytebucket.org/ravatar/%7B0301417f-87a0-4c8d-8f45-969db3a9eb7a%7D?ts=2135667'
repositoryPath: 'WhitesourceSoftware/whitesource-scan'
version: '1.1.0'
vendor:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
maintainer:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
"""

MANIFEST_NO_CATEGORY_FIXTURE = """name: WhiteSource scan
description: Scan and report open sources vulnerabilities and security issues
logo: 'https://bytebucket.org/ravatar/%7B0301417f-87a0-4c8d-8f45-969db3a9eb7a%7D?ts=2135667'
repositoryPath: 'WhitesourceSoftware/whitesource-scan'
version: '1.1.0'
vendor:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
maintainer:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
"""

MANIFEST_NON_EXISTENT_CATEGORY = """name: WhiteSource scan
category: Fake
description: Scan and report open sources vulnerabilities and security issues
logo: 'https://bytebucket.org/ravatar/%7B0301417f-87a0-4c8d-8f45-969db3a9eb7a%7D?ts=2135667'
repositoryPath: 'WhitesourceSoftware/whitesource-scan'
version: '1.1.0'
vendor:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
maintainer:
  name: WhiteSource
  website: https://www.whitesourcesoftware.com/
"""

MANIFEST_FOR_AZURE_ICON_REMOTE = """
name: Azure Web Apps containers deploy
description: Deploy a container to Azure Web Apps.
category: Deployment
logo: 'https://bytebucket.org/ravatar/%7B96fc02dc-e1f7-41fe-a7c9-f27cef1480b3%7D?ts=2135670'
repositoryPath: 'microsoft/azure-web-apps-containers-deploy'
version: '1.0.2'
vendor:
  name: Microsoft
  website: https://www.microsoft.com/
maintainer:
  name: Microsoft
  website: https://www.microsoft.com/
"""


class BuildE2ERealTest(unittest.TestCase):
    def test_real_check_new_structure(self):

        context = build.extract_information(yaml.safe_load(MANIFEST_FIXTURE), 'pipes/ssh-run.yml')
        failures = build.validate(context)
        self.assertEqual(len(failures.criticals), 0)
        self.assertEqual(len(failures.warnings), 1)
        self.assertRegexpMatches(failures.warnings[0], r'\d+.\d+.\d+ version available. Current version is 0.2.6')

    def test_pipe_deprecated_structure(self):

        context = build.extract_information(
            yaml.safe_load(MANIFEST_CUSTOM_PIPE_OLD_METADATA_FIXTURE),
            'pipes/whitesource-scan.yml'
        )
        failures = build.validate(context)
        self.assertEqual(len(failures.criticals), 0)
        self.assertEqual(len(failures.warnings), 1)

    def test_pipe_deprecated_structure_icon(self):
        context = build.extract_information(yaml.safe_load(MANIFEST_FOR_AZURE_ICON_REMOTE),
                                            'pipes/azure-web-apps-containers-deploy.yml')
        failures = build.validate(context)
        self.assertEqual(0, len(failures.criticals))
        self.assertEqual(0, len(failures.warnings))

    def test_category_not_found(self):
        context = build.extract_information(yaml.safe_load(MANIFEST_NO_CATEGORY_FIXTURE), 'pipes/whitesource-scan.yml')

        failures = build.validate(context)
        self.assertEqual(1, len(failures.criticals))
        self.assertTrue(str(failures.criticals[0]).find("{'category': ['required field']}"))

    def test_category_not_allowed(self):
        context = build.extract_information(yaml.safe_load(MANIFEST_NON_EXISTENT_CATEGORY), 'pipes/whitesource-scan.yml')

        failures = build.validate(context)
        self.assertEqual(2, len(failures.criticals))

        self.assertTrue(str(failures.criticals[0]).find("unallowed value Fake"))
        self.assertTrue(str(failures.criticals[1]).find("unallowed value Fake"))
